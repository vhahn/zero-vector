shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_toon,specular_toon;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_metallic : hint_white;
uniform vec4 metallic_texture_channel;
uniform sampler2D texture_roughness : hint_white;
uniform vec4 roughness_texture_channel;
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;



void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
	float metallic_tex = dot(texture(texture_metallic,base_uv),metallic_texture_channel);
	METALLIC = metallic_tex * metallic;
	float roughness_tex = dot(texture(texture_roughness,base_uv),roughness_texture_channel);
	ROUGHNESS = roughness_tex * roughness;
	SPECULAR = specular;
}

void light() {
	vec4 dark_color = vec4(0.0,0.01,0.02,1.0);
	vec4 bright_color = vec4(1.0,1.0,0.7,1.0);
	vec3 light = LIGHT_COLOR * max(dot(NORMAL, LIGHT),0.0) * ATTENUATION * ALBEDO;
	float brightness = max(dot(NORMAL, LIGHT_COLOR * LIGHT * ATTENUATION),0.0);
	float w_dark = clamp(1.0-3.0*round(4.0*brightness*dark_color.a)/4.0,0.0,1.0);
	float w_bright = clamp(3.0*round(4.0*brightness*bright_color.a)/4.0-2.0,0.0,1.0);
	light = round(3.0*light)/3.0;
//	light = (light+vec3(0.5))/2.0;
//	light = (1.0-w_dark)*light+w_dark*dark_color.rgb;
	light = (1.0-w_bright)*light+w_bright*bright_color.rgb;
	DIFFUSE_LIGHT += light;
}
