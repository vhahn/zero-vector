# Zero Vector

Racing game prototype.
The game is in an early stage. You can't do anything else than to drive on a randomly generated endless track. Bots will respawn automatically.

These are the project files for Godot 3.1. It's meant to be opened or run using Godot's project manager. See https://godotengine.org/

