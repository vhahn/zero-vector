extends Node

const VOVELS = ["a","e","o","u","i"]
const CONS = ["b","c","d","f","g","h","j","k","l","m","n","p","r","s","t","v","w","x","y","z"]
const PHRASE_MALE = ["cer","re","zer","ice","er","bu","lon","arc","cade","ave","dan","or","ex","us","or","er","ion"]
const PHRASE_FEMALE = ["re","ra","ice","ey","arc","dia","cade","ria","ave","dan","ise","ex","ia","ion","as","you","ina"]
const NAMES_FIRST_MALE = ["curly","billy","isaac","harry","raymond","julian"]
const NAMES_FIRST_FEMALE = ["curly","kitty","brandy","kaylee","shakira","lyra"]
const NAMES_LAST = ["test","chandra","unknown","wisdom","fortune","intelligence","knowledge","power","useless"]


func randomize_name(name,phrases):
	name = random_syllable(name)
	for _i in range(randi()%int(1+0.25*name.length())+1):
		name = replace_syllable(name,phrases)
	return name.capitalize()

func random_name_tags(list,phrases):
	if list.size()<1:
		return ""
	var name = random_name_conc(list[randi()%list.size()],list[randi()%list.size()],phrases).capitalize()
	return name

func random_name_conc(str1,str2,phrases):
	var name = conc(str1,str2)
	
	for _i in range(randi()%int(1+0.25*name.length())+1):
		name = replace_syllable(name,phrases)
	return name

func conc(str1,str2):
	var c1 = clamp(round(str1.length()*rand_range(0.4,0.6)+rand_range(-1.5,1.5)),1,str1.length()-1)
	var c2 = clamp(round(str2.length()*rand_range(0.4,0.6)+rand_range(-1.5,1.5)),0,str2.length()-2)
	var name = str1.substr(0,c1)
	if !(str1[max(c1-1,0)] in VOVELS) && !(str2[c2] in VOVELS) && (!(str1[max(c1-2,0)] in VOVELS) || !(str2[min(c2+1,str2.length()-1)] in VOVELS)):
		name += VOVELS[randi()%(VOVELS.size())]
	name += str2.substr(c2,str2.length()-c2)
	return name

func replace_syllable(name,phrases):
	var pos
	var phrase = phrases[randi()%phrases.size()]
	var length = phrase.length()
	for _i in range(20):
		pos = randi()%(name.length())
		if name[pos] in VOVELS:
			break
	if randi()%6+4<name.length():
		length += 1
	if pos+length>name.length()-1:
		pos -= pos+length-name.length()+1
	name = name.substr(0,pos)+phrase+name.substr(pos+length,name.length()-pos-length)
	for i in range(max(pos-1,0),min(pos+length+1,name.length()-1)):
		if !(name[clamp(i,0,name.length()-1)] in VOVELS) && !(name[clamp(i-1,0,name.length()-1)] in VOVELS) && !(name[clamp(i+1,0,name.length()-1)] in VOVELS):
			name[i] = VOVELS[randi()%VOVELS.size()]
		elif (name[clamp(i,0,name.length()-1)] in VOVELS) && (name[clamp(i-1,0,name.length()-1)] in VOVELS) && (name[clamp(i+1,0,name.length()-1)] in VOVELS):
			name[i] = CONS[randi()%CONS.size()]
	return name

func random_syllable(name):
	var pos
	var dir
	var length
	for _i in range(20):
		pos = randi()%(name.length())
		if (name[pos] in VOVELS):
			break
	if (pos==0):
		dir = 1
	elif (pos==name.length()-1):
		dir = -1
	else:
		dir = 2*(randi()%2)-1
	length = 1+randi()%3
	if (pos+dir*length<0):
		length += pos+dir*length
	elif (pos+dir*length>name.length()-1):
		length -= pos+dir*length-name.length()+1
	for i in range(pos,pos+dir*length,dir):
		name = name.substr(0,i)+CONS[randi()%CONS.size()]+name.substr(i+1,name.length()-1-i)
	for i in range(name.length()):
		if (!(name[clamp(i,0,name.length()-1)] in VOVELS) && !(name[clamp(i-1,0,name.length()-1)] in VOVELS) && !(name[clamp(i+1,0,name.length()-1)] in VOVELS)):
			name[i] = VOVELS[randi()%VOVELS.size()]
	return name

func random_name(female):
	if female:
		return random_name_tags(NAMES_FIRST_FEMALE,PHRASE_FEMALE)+" "+randomize_name(NAMES_LAST[randi()%NAMES_LAST.size()],PHRASE_MALE+PHRASE_FEMALE)
	else:
		return random_name_tags(NAMES_FIRST_MALE,PHRASE_MALE)+" "+randomize_name(NAMES_LAST[randi()%NAMES_LAST.size()],PHRASE_MALE+PHRASE_FEMALE)

