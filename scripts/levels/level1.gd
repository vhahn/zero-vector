extends Spatial

const TRANSITION_PROB = {
	"start":{"straight":1.0},
	"straight":{"straight":0.45,"curve_left":0.075,"curve_right":0.075,"ramp":0.1,"straight_up":0.1,"straight_down":0.1,"sharp_left":0.05,"sharp_right":0.05},
	"curve_left":{"straight":0.85,"curve_right":0.15},
	"curve_right":{"straight":0.85,"curve_left":0.15},
	"sharp_left":{"straight":0.8,"straight_up":0.1,"straight_down":0.1},
	"sharp_right":{"straight":0.8,"straight_up":0.1,"straight_down":0.1},
	"ramp":{"straight":0.8,"ramp":0.2},
	"up":{"up":0.4,"up_straight":0.6},
	"down":{"down_straight":1.0},
	"straight_up":{"up":0.75,"up_straight":0.25},
	"straight_down":{"down":0.5,"down_straight":0.5},
	"down_straight":{"straight":1.0},
	"up_straight":{"straight":1.0}
}

# warning-ignore:unused_class_variable
var gravity = Vector3(0,-9.8*2,0)

var player
var current_pos = Vector3(0,0,0)
var current_rot = Vector3(0,0,0)
var current_type = "start"
var num = 0
var waypoints = []
var min_y = 0.0
var players = []

onready var camera = $Camera

var player_scene = preload("res://scenes/player/player.tscn")
var script_bot = preload("res://scripts/player/ai.gd")
# warning-ignore:unused_class_variable
var test_enemy = preload("res://scenes/enemies/test.tscn")
var gui_panel = preload("res://scenes/gui/player.tscn")
# warning-ignore:unused_class_variable
var segments_straight = [
	preload("res://scenes/levels/segment1.tscn"),preload("res://scenes/levels/segment1.tscn"),
	preload("res://scenes/levels/segment2.tscn"),preload("res://scenes/levels/segment3.tscn")
]
# warning-ignore:unused_class_variable
var segments_ramp = [
	preload("res://scenes/levels/ramp1.tscn")
]
# warning-ignore:unused_class_variable
var segments_curve_left = [
	preload("res://scenes/levels/curve_left.tscn")
]
# warning-ignore:unused_class_variable
var segments_curve_right = [
	preload("res://scenes/levels/curve_right.tscn")
]
# warning-ignore:unused_class_variable
var segments_sharp_left = [
	preload("res://scenes/levels/sharp_left.tscn")
]
# warning-ignore:unused_class_variable
var segments_sharp_right = [
	preload("res://scenes/levels/sharp_right.tscn")
]
# warning-ignore:unused_class_variable
var segments_up = [
	preload("res://scenes/levels/diag_up.tscn")
]
# warning-ignore:unused_class_variable
var segments_down = [
	preload("res://scenes/levels/diag_down.tscn")
]
# warning-ignore:unused_class_variable
var segments_straight_up= [
	preload("res://scenes/levels/segment_up.tscn")
]
# warning-ignore:unused_class_variable
var segments_straight_down = [
	preload("res://scenes/levels/segment_down.tscn")
]
# warning-ignore:unused_class_variable
var segments_up_straight= [
	preload("res://scenes/levels/segment_up_straight.tscn")
]
# warning-ignore:unused_class_variable
var segments_down_straight = [
	preload("res://scenes/levels/segment_down_straight.tscn")
]
# warning-ignore:unused_class_variable
var environment_straight = [
	preload("res://scenes/levels/canyon_straight.tscn"),preload("res://scenes/levels/canyon_straight.tscn"),
	preload("res://scenes/levels/canyon_straight2.tscn"),
]
# warning-ignore:unused_class_variable
var environment_ramp = [
	preload("res://scenes/levels/canyon_ramp.tscn")
]
# warning-ignore:unused_class_variable
var environment_curve_left = [
	preload("res://scenes/levels/canyon_curve_left.tscn"),preload("res://scenes/levels/canyon_curve_left2.tscn")
]
# warning-ignore:unused_class_variable
var environment_curve_right = [
	preload("res://scenes/levels/canyon_curve_right.tscn"),preload("res://scenes/levels/canyon_curve_right2.tscn")
]
# warning-ignore:unused_class_variable
var environment_sharp_left = [
	preload("res://scenes/levels/canyon_sharp_left.tscn")
]
# warning-ignore:unused_class_variable
var environment_sharp_right = [
	preload("res://scenes/levels/canyon_sharp_right.tscn")
]
# warning-ignore:unused_class_variable
var environment_up = [
	preload("res://scenes/levels/canyon_up.tscn")
]
# warning-ignore:unused_class_variable
var environment_down = [
	preload("res://scenes/levels/canyon_down.tscn")
]
# warning-ignore:unused_class_variable
var environment_straight_up = [
	preload("res://scenes/levels/canyon_straight_up.tscn")
]
# warning-ignore:unused_class_variable
var environment_straight_down = [
	preload("res://scenes/levels/canyon_straight_down.tscn")
]
# warning-ignore:unused_class_variable
var environment_up_straight = [
	preload("res://scenes/levels/canyon_up_straight.tscn")
]
# warning-ignore:unused_class_variable
var environment_down_straight = [
	preload("res://scenes/levels/canyon_down_straight.tscn")
]


class RankSorter:
	static func sort(a, b):
		if a[1]>b[1]:
			return true
		return false


func get_next_type(current):
	var p = 0.0
	var rnd = randf()
	var type = "straight"
	for i in range(TRANSITION_PROB[current].size()):
		if rnd<=TRANSITION_PROB[current].values()[i]+p:
			type = TRANSITION_PROB[current].keys()[i]
			return type
		else:
			p += TRANSITION_PROB[current].values()[i]
	return type

func add_segment():
	var seg
	var env
	current_type = get_next_type(current_type)
	seg = get("segments_"+current_type)[randi()%get("segments_"+current_type).size()].instance()
	seg.translation = current_pos
	seg.rotation = current_rot
	
	env = get("environment_"+current_type)[randi()%get("environment_"+current_type).size()].instance()
	env.translation = current_pos
	env.rotation = current_rot
	$Environment.add_child(env)
	
	current_pos += seg.offset.rotated(Vector3(1,0,0),current_rot.x).rotated(Vector3(0,1,0),current_rot.y).rotated(Vector3(0,0,1),current_rot.z)
	current_rot += seg.rot*PI/180.0
	$Track.add_child(seg)
	for wp in seg.waypoints:
		waypoints.push_back(seg.transform.xform(wp))
	if current_pos.y<min_y:
		min_y = current_pos.y
	num += 1
	
#	if randf()<0.1:
#		var ei = test_enemy.instance()
#		ei.translation = current_pos+Vector3(rand_range(-3.5,3.5),0.5,rand_range(-3.5,3.5))
#		ei.rotation = PI*Vector3(randf(),randf(),randf())
#		add_child(ei)
	return seg

func add_ai():
	var pos
	var dir
	var ID = 0
	var min_dist2 = 1000000
	var bot = player_scene.instance()
	var panel = gui_panel.instance()
	
	if player!=null:
		for i in range(waypoints.size()):
			var wp = waypoints[i]
			var dist2 = player.translation.distance_squared_to(wp)
			if dist2<min_dist2:
				min_dist2 = dist2
				ID = i
	ID = clamp(ID+10,0,waypoints.size()-1)
	pos = waypoints[ID]
	dir = (waypoints[min(ID+1,waypoints.size()-1)]-waypoints[ID]).normalized()
	
	bot.script = script_bot
	bot.translation = pos+Vector3(rand_range(-4.0,4.0),1.0,rand_range(-4.0,4.0))
	bot.nm = Names.random_name(randi()%2)
	bot.gui_panel = panel
	panel.get_node("LabelN1").text = bot.nm.split(" ")[0]
	for _i in range(bot.nm.split(" ")[1].length()):
		panel.get_node("LabelN1").text += "  "
	panel.get_node("LabelN2").text = bot.nm.split(" ")[1]
	for _i in range(bot.nm.split(" ")[0].length()):
		panel.get_node("LabelN2").text = " "+panel.get_node("LabelN2").text
	panel.get_node("Rank").text = str(players.size())
	panel.rect_position = Vector2(256,0)
	GUI.get_node("Players").add_child(panel)
	add_child(bot)
	
	players.push_back(bot)
	printt("ADDED",bot.nm)
	return bot


func get_pos(c):
	var closest_wp = 0
	var closest_dist2 = 1000000
	var next_wp = 0
	var next_dist2 = 1000000
	for i in range(waypoints.size()):
		var wp = waypoints[i]
		var dist2 = c.translation.distance_squared_to(wp)
		if dist2<closest_dist2:
			next_wp = closest_wp
			next_dist2 = closest_dist2
			closest_dist2 = dist2
			closest_wp = i
	
	return closest_wp+(next_wp-closest_wp)*clamp(closest_dist2/(next_dist2+1.0),0.0,1.0)

func sort_rank():
	var array = []
	array.resize(players.size())
	for i in range(players.size()):
		array[i] = [players[i],get_pos(players[i])]
	array.sort_custom(RankSorter,"sort")
	return array

func sort_panels(sorted):
	for i in range(sorted.size()):
		var l = sorted[i][0].gui_panel.rect_position.y/32.0
		sorted[i][0].gui_panel.pos = Vector2(-64.0*float(i<l),32.0*i)



func _process(delta):
	if player==null:
		return
	
	camera.translation = camera.translation+delta*20.0*(player.translation+0.75*player.transform.basis.z+Vector3(0,0.5,0)-camera.translation)
	camera.look_at(player.translation,Vector3(0,1,0))
	
	if player.translation.distance_squared_to(current_pos)<90000.0:
		add_segment()
		
		for p in players:
			if p.translation.y<min_y-25.0 || player.translation.distance_squared_to(p.translation)>180000.0:
				if p.disabled:
					players.erase(p)
					printt("REMOVED",p.nm)
					p.gui_panel.queue_free()
					p.queue_free()
				p.disable()
		if players.size()<3+randi()%5:
			add_ai()
		var sorted = sort_rank()
		for i in range(sorted.size()):
			sorted[i][0].gui_panel.get_node("Rank").text = str(i+1)
#			sorted[i][0].gui_panel.rect_position.y = 32*i
		sort_panels(sorted)
	for c in $Track.get_children():
		if player.translation.distance_squared_to(c.translation)>360000.0:
			for wp in c.waypoints:
				waypoints.erase(wp)
			c.queue_free()
	for c in $Environment.get_children():
		if player.translation.distance_squared_to(c.translation)>360000.0:
			c.queue_free()
	

func _ready():
	randomize()
	
	add_segment()
#	player = add_ai()
#	return
	
	var panel = gui_panel.instance()
	player = player_scene.instance()
	player.translation = Vector3(0,0.5,0)
	player.nm = "Player"
	player.gui_panel = panel
	panel.get_node("LabelN1").text = player.nm.split(" ")[0]
	panel.get_node("LabelN2").text = ""
	panel.get_node("Rank").text = "1"
	panel.rect_position = Vector2(256,0)
	GUI.get_node("Players").add_child(panel)
	add_child(player)
	players.push_back(player)
	camera.make_current()
	
#	var wrack = preload("res://scenes/player/wrack.tscn")
#	var wi = wrack.instance()
#	wi.translation = Vector3(rand_range(-2,2),2,rand_range(-8,0))
#	add_child(wi)
