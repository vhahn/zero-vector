extends CanvasLayer

var pixel_size = 4
var level

onready var canvas = $ViewportContainer
onready var viewport = $ViewportContainer/Viewport


func _resized():
#	pixel_size = 2.0*max(OS.window_size.x,OS.window_size.y)/1024.0
#	canvas.anchor_right = 1.0/pixel_size
#	canvas.anchor_bottom = 1.0/pixel_size
#	canvas.margin_right = 0
#	canvas.margin_bottom = 0
#	canvas.rect_size = OS.window_size/pixel_size
#	canvas.rect_scale = pixel_size*Vector2(1,1)
	pixel_size = 1.0
	canvas.anchor_right = 1.0
	canvas.anchor_bottom = 1.0
	canvas.margin_right = 0
	canvas.margin_bottom = 0
	canvas.rect_size = OS.window_size
	canvas.rect_scale = Vector2(1,1)
	$ViewportContainer/Viewport.size = OS.window_size

func _ready():
	get_tree().connect("screen_resized",self,"_resized")
	_resized()
	level = preload("res://scenes/levels/level1.tscn").instance()
	$ViewportContainer/Viewport.add_child(level)
