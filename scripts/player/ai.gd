extends "res://scripts/player/base.gd"

onready var raycast = $RayCastCollision


func _process(_delta):
	var dir
	var vdir
	var ID = 0
	var closest_segment_pos = translation
	var closest_segment_dist2 = 1000000
	var next_segment_pos = translation+transform.basis.z
	var next_segment_dist2 = 1000000000
	
	for i in range(get_parent().waypoints.size()):
		var wp = get_parent().waypoints[i]
		var dist2 = translation.distance_squared_to(wp)
		if dist2<closest_segment_dist2:
			closest_segment_pos = wp
			closest_segment_dist2 = dist2
			ID = i
	for i in range(ID,get_parent().waypoints.size()):
		var wp = get_parent().waypoints[i]
		var dist2 = (translation-24.0*transform.basis.z).distance_squared_to(wp)
		if dist2<next_segment_dist2 && closest_segment_pos!=wp:
			next_segment_pos = wp
			next_segment_dist2 = dist2
	dir = ((closest_segment_pos+translation)/2.0-next_segment_pos).normalized()
	vdir = (transform.xform(linear_velocity)+transform.basis.z).normalized()
	
	move = Vector2(clamp(-8.0*transform.basis.x.dot(dir)*(1.5-abs(vdir.dot(dir))),-1.0,1.0),clamp(-3.0*transform.basis.z.dot(dir)*abs(vdir.dot(dir))-1.0,-1.0,1.0))#.normalized()
	move.y = min(move.y+max(move.x*move.x-0.125,0.0),1.0)
	if raycast.is_colliding():
		if move.x>0.0:
			roll(-1)
		else:
			roll(1)
	

func _ready():
	raycast.enabled = true
