extends KinematicBody

var ACCEL = 14.0

# warning-ignore:unused_class_variable
var nm = ""
var linear_velocity = Vector3()
var facing = transform.basis.z
var up = Vector3(0,1,0)
var move = Vector2()
var shield = 100
var max_shield = 100
var recharge = 1.0
var trail_delay = 0.0
var trail1_points = []
var trail2_points = []
var time = 0.0
var last_pos = translation
var last_lv = linear_velocity
var roll_up = Vector3()
var roll_side = Vector3()
var rolling = 0.0
var roll_time = 0.0
var roll_dir = 1
var disabled = false
var collided = 0.0
var spark_timer = 0.0
var gui_panel

onready var trail = $Trail
onready var sound_engine = $AudioEngine
onready var sound_acc = $AudioAccelerate

var wrack = preload("res://scenes/player/wrack.tscn")


func disable():
	if disabled:
		return
	
	var wi = wrack.instance()
	disabled = true
	set_physics_process(false)
	set_process(false)
	hide()
	collision_layer = 0
	collision_mask = 0
	sound_engine.playing = false
	sound_acc.playing = false
	wi.set_global_transform(get_global_transform())
	wi.apply_impulse(Vector3(rand_range(-0.15,0.15),rand_range(-0.05,0.05),rand_range(-0.05,0.05)),10*linear_velocity+Vector3(0,10,0))
	get_parent().add_child(wi)
	if has_method("_disabled"):
		call("_disabled",wi)
	gui_panel.get_node("Dead").show()
	gui_panel.get_node("Critical").hide()
	gui_panel.get_node("New").hide()
	wi.apply_central_impulse(linear_velocity+Vector3(0,10,0))

func roll(dir):
	if rolling>-2.0:
		return
	
	roll_time = 1.0
	rolling = roll_time
	roll_dir = dir
	roll_up = up
	roll_side = transform.basis.x

func damaged(dam):
	shield -= dam
	collided = 0.25
	if shield<0.25*max_shield:
		gui_panel.get_node("Critical").show()
		gui_panel.get_node("New").hide()

func get_velocity():
	return linear_velocity


func _physics_process(delta):
	var state
	var gravity = get_parent().gravity
	var normal = Vector3()
	var min_dist2 = 1.0
	
	rolling -= delta
	linear_velocity += delta*ACCEL*(move.y*transform.basis.z+0.5*move.x*transform.basis.x)
	linear_velocity += delta*gravity
	
	for dir in ["Front","Back","Left","Right"]:
		var raycast = get_node("RayCast"+dir)
		if raycast.is_colliding():
			var norm = raycast.get_collision_normal()
			var point = raycast.get_collision_point()
			var dist2 = raycast.get_global_transform().origin.distance_squared_to(point)
			normal += norm
			linear_velocity -= min(linear_velocity.dot(norm),0.0)*1.1*norm*max(0.75-dist2,0.0)
			if dist2<min_dist2:
				min_dist2 = dist2
	if normal==Vector3():
		normal = -gravity.normalized()
	else:
		normal = normal.normalized()
	linear_velocity += (delta*20.0-delta*20.0*min(linear_velocity.dot(normal),0.0))*max(0.5-min_dist2,0.0)*max(1.0-min_dist2,0.0)*normal
	
	if rolling>0.0:
		facing = 0.9*facing+0.1*transform.basis.z
	else:
		facing = 0.9*facing-0.1*(normal.cross(transform.basis.x))
	facing = facing.rotated(Vector3(0,1,0),-delta*0.75*move.x)
	up = (0.98*up+0.02*normal).normalized()
	if rolling>0.0 && roll_time>0.0:
		up = roll_up.rotated(transform.basis.z,-2.0*PI*roll_dir*rolling/roll_time)
		linear_velocity += delta*3.0*ACCEL*normal*(1.0-sin(PI*rolling/roll_time))-delta*3.0*ACCEL*roll_dir*roll_side*rolling*rolling/roll_time/roll_time
	look_at(translation-facing,up)
	
	state = move_and_collide(delta*linear_velocity)
	linear_velocity *= 0.995
	
	if state!=null:
		var lv = (last_pos-translation)/delta
		var lvc = Vector3(0,0,0)
		var n2 = (state.position-translation).normalized()
		if state.get_collider().has_method("get_velocity"):
			lvc = state.get_collider().get_velocity()
		linear_velocity = (linear_velocity + linear_velocity.reflect(transform.xform(state.normal).normalized()))/2.0
		linear_velocity -= 1.1*(linear_velocity-lvc).dot(n2)*n2
		if collided<=0.0:
			var dam = 0.0
			if state.get_collider().has_method("impact_damage"):
				dam += state.get_collider().impact_damage(lv)
			if state.get_collider().has_method("get_velocity"):
				dam += min(2.0*abs(state.normal.dot(lv-lvc.normalized()*state.normal.dot(lvc))),25)
			else:
				dam += min(4.0*abs(state.normal.dot(lv)),30)
			damaged(dam)
			if state.get_collider().has_method("damaged"):
				state.get_collider().damaged(dam)
		$Sparks.set_global_transform(Transform(Vector3(1,0,0),Vector3(0,1,0),Vector3(0,0,1),state.position))
		$Sparks.look_at(translation+linear_velocity,Vector3(0,1,0))
		$Sparks.process_material.initial_velocity = rand_range(0.8,1.1)*translation.distance_to(last_pos)/delta
		$Sparks.emitting = true
		spark_timer = 0.1
# warning-ignore:return_value_discarded
		move_and_collide(delta*linear_velocity)
		lv = (last_pos-translation)/delta
		linear_velocity = 0.95*linear_velocity+0.05*lv
	
	last_pos = translation
	last_lv = linear_velocity
	

func _process(delta):
	var transform_inv = transform.inverse()
	
	if move.x==0:
		move.x = Input.get_joy_axis(0, JOY_AXIS_0)
	
	time += delta
	shield += delta*recharge
	collided -= delta
	gui_panel.get_node("Critical").visible = shield<0.25*max_shield && !disabled
	if shield>=max_shield:
		shield = max_shield
	elif shield<=0:
		disable()
	
	$RayCastLeft.transform = transform_inv
	$RayCastLeft.translation = Vector3(0,0,0)
	$RayCastRight.transform = transform_inv
	$RayCastRight.translation = Vector3(0,0,0)
	$RayCastFront.transform = transform_inv
	$RayCastFront.translation = Vector3(0,0,0)
	$RayCastBack.transform = transform_inv
	$RayCastBack.translation = Vector3(0,0,0)
	
	$EngineLeft/Light.visible = move!=Vector2()
	$EngineRight/Light.visible = move!=Vector2()
	
	trail_delay -= delta
	if trail_delay<0.0:
		trail1_points.push_back($EngineLeft.get_global_transform().origin)
		trail2_points.push_back($EngineRight.get_global_transform().origin)
		if trail1_points.size()>20:
			trail1_points.pop_front()
		if trail2_points.size()>20:
			trail2_points.pop_front()
		trail_delay += 0.0025
	
	trail.clear()
	trail.begin(Mesh.PRIMITIVE_TRIANGLE_FAN, null)
	for p in trail1_points:
		for i in range(3):
			trail.add_vertex(p-translation+rand_range(0.015,0.03)*Vector3(1,1,0).rotated(transform.basis.z,2.0*PI*(i/3.0+11.7*time)))
	trail.end()
	trail.begin(Mesh.PRIMITIVE_TRIANGLE_FAN, null)
	for p in trail2_points:
		for i in range(3):
			trail.add_vertex(p-translation+rand_range(0.015,0.03)*Vector3(1,1,0).rotated(transform.basis.z,2.0*PI*(i/3.0+11.7*time)))
	trail.end()
	trail.transform = transform_inv
	trail.translation = Vector3(0,0,0)
	$Thrust.clear()
	if rolling>0.0:
		var pos_l = $EngineLeft.translation
		var pos_r = $EngineRight.translation
		$Thrust.begin(Mesh.PRIMITIVE_TRIANGLE_FAN, null)
		for i in range(3):
			$Thrust.add_vertex(pos_l+0.04*Vector3(cos(2.0*PI*(i/3.0+11.7*time)),0,sin(2.0*PI*(i/3.0+11.7*time))))
		for _i in range(3):
			$Thrust.add_vertex(pos_l+Vector3(0,1,0)*roll_dir/3.0)
		$Thrust.end()
		$Thrust.begin(Mesh.PRIMITIVE_TRIANGLE_FAN, null)
		for i in range(3):
			$Thrust.add_vertex(pos_r+0.04*Vector3(cos(2.0*PI*(i/3.0+11.7*time)),0,sin(2.0*PI*(i/3.0+11.7*time))))
		for _i in range(3):
			$Thrust.add_vertex(pos_r-Vector3(0,1,0)*roll_dir/3.0)
		$Thrust.end()
	
	spark_timer -= delta
	if spark_timer<0.0:
		$Sparks.emitting = false
	
	sound_engine.unit_db = min(linear_velocity.length_squared()/30.0-20.0,0.0)
	sound_engine.pitch_scale = linear_velocity.length_squared()/1000.0+0.5
	sound_acc.unit_db = sound_acc.unit_db+delta*5.0*(clamp(15.0*max(linear_velocity.length_squared()-last_lv.length_squared()-0.5,0.0)+5.0*abs(linear_velocity.cross(last_lv).length_squared())-20.0,-30.0,-2.0)-sound_acc.unit_db)
	sound_acc.pitch_scale = linear_velocity.length_squared()/1000.0+0.5
	

func _ready():
	var timer = Timer.new()
	last_pos = translation
	timer.wait_time = 10.0
	timer.one_shot = true
	add_child(timer)
	timer.start()
	yield(timer,"timeout")
	gui_panel.get_node("New").hide()
