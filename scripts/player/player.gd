extends "res://scripts/player/base.gd"


func _disabled(wrack):
	get_parent().player = wrack

func _input(event):
	move = Vector2(float(Input.is_action_pressed("strafe_right"))-float(Input.is_action_pressed("strafe_left")),float(Input.is_action_pressed("reverse_thrust"))-float(Input.is_action_pressed("thrust")))#.normalized()
	if event.is_action_pressed("roll_left"):
		roll(1)
	elif event.is_action_pressed("roll_right"):
		roll(-1)
	elif event.is_action_pressed("menu"):
		if GUI.get_node("Menu").visible:
			GUI.get_node("AnimationPlayer").play("hide_menu")
		else:
			GUI.get_node("AnimationPlayer").play("show_menu")

func _process(_delta):
	var player_str = "players: "
	for p in get_parent().players:
		player_str += p.nm+"\n         "
	GUI.get_node("Label").text = "rotation: "+str(rotation/PI*180)+"\nbasis.x: "+str(transform.basis.x)+"\nbasis.y: "+str(transform.basis.y)+"\nbasis.z: "+str(transform.basis.z)+"\naccelerate sound db: "+str($AudioAccelerate.unit_db)+"\n"+player_str
#	GUI.get_node("LabelSpeed").text = str(last_pos.distance_to(translation)/delta).pad_decimals(1)+" m/s"
	GUI.get_node("LabelSpeed").text = str(linear_velocity.length()).pad_decimals(1)+" m/s"
	GUI.get_node("Shield").value = shield
	GUI.get_node("Shield").max_value = max_shield
	

func _ready():
	nm = "Player"
