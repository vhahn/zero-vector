extends CanvasLayer

var thread


func load_map(level):
	var li = load("res://scenes/levels/"+level[0])
	call_deferred("load_done",li)
	return li

func load_level(level):
	thread = Thread.new()
	thread.start(self,"load_map",[level],Thread.PRIORITY_HIGH)

func load_done(level):
# warning-ignore:return_value_discarded
	get_tree().change_scene_to(level)
	GUI.show()
	hide()

func hide():
	for c in get_children():
		if c.has_method("hide"):
			c.hide()

func _ready():
	load_level("level1.tscn")
