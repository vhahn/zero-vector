extends CanvasLayer


func hide():
	for c in get_children():
		if c.has_method("hide"):
			c.hide()

func show():
	for c in get_children():
		if c.has_method("show"):
			c.show()
	$Label.hide()
	$Black.hide()
	$Menu.hide()

func restart():
	for c in $Players.get_children():
		c.queue_free()
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/levels/level1.tscn")
	$AnimationPlayer.play("hide_menu")

func quit():
	get_tree().quit()

func _ready():
# warning-ignore:return_value_discarded
	$Menu/VBoxContainer/ButtonC.connect("pressed",$AnimationPlayer,"play",["hide_menu"])
# warning-ignore:return_value_discarded
	$Menu/VBoxContainer/ButtonR.connect("pressed",self,"restart")
# warning-ignore:return_value_discarded
	$Menu/VBoxContainer/ButtonQ.connect("pressed",self,"quit")
	
	hide()
