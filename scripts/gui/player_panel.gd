extends TextureRect

const SPEED = 8.0

onready var pos = rect_position

func _process(delta):
	if pos.x<rect_position.x:
		if abs(rect_position.x-pos.x)<delta*2.0*SPEED:
			rect_position.x = pos.x
		else:
			rect_position.x = rect_position.x+delta*2.0*SPEED*(pos.x-rect_position.x)
	elif pos.y<rect_position.y:
		if abs(rect_position.y-pos.y)<=delta*SPEED:
			rect_position.y = pos.y
			pos.x = 0
		else:
			rect_position.y = rect_position.y+delta*SPEED*(pos.y-rect_position.y)
	else:
		if rect_position.distance_squared_to(pos)<delta*delta*SPEED*SPEED:
			rect_position = pos
		else:
			rect_position = rect_position+delta*SPEED*(pos-rect_position)
